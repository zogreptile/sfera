
var gulp = require('gulp'),
	less = require('gulp-less'),
	rename = require('gulp-rename'),
	bs = require('browser-sync').create(),
	autoprefixer = require('gulp-autoprefixer'),
	cssmin = require('gulp-cssmin'),
	combineMq = require('gulp-combine-mq'),
	sourcemaps = require('gulp-sourcemaps'),
	nunjucks = require('gulp-nunjucks'),
	data = require('gulp-data'),
	prettify = require('gulp-html-prettify');;

var paths = {
	src: {
		nunjucks: "src/nunjucks",
		less: "src/less"
	},
	dist: {
		html: "dist",
		css: "dist/css",
	}
};

gulp.task('nunjucks', function() {
	gulp.src(paths.src.nunjucks + '/*.html')
		.pipe(data(function() {
			return require('./src/db.json');
		}))
		.pipe(nunjucks.compile())
		.pipe(prettify({indent_char: '\t', indent_size: 1}))
		.pipe(gulp.dest(paths.dist.html))
		.pipe(bs.reload({ stream: true }));
	}
);

gulp.task('less', function() {
	return gulp.src(paths.src.less + '/styles.less')
		.pipe(sourcemaps.init())
		.pipe(less())
		.pipe(sourcemaps.write())
		.pipe(rename('style.css'))
		.pipe(gulp.dest(paths.dist.css))
		.pipe(bs.reload({stream: true}));
});

gulp.task('browser-sync', function() {
	bs.init({
		server: {
			baseDir: './dist'
		}
	});
});

gulp.task('autoprefixer', function() {
	gulp.src(paths.dist.css + '/style.css')
		.pipe(autoprefixer({
			browsers: ['last 8 versions'],
			cascade: false
		}))
		.pipe(gulp.dest(paths.dist.css));
});

gulp.task('cssmin', function () {
	gulp.src(paths.dist.css + '/style.css')
		.pipe(cssmin())
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulp.dest(paths.dist.css));
});

gulp.task('css-ap-com', function() {
	gulp.src(paths.dist.css + '/style.css')
		.pipe(autoprefixer({
			browsers: ['last 8 versions'],
			cascade: false
		}))
		.pipe(combineMq({
			beautify: true
		}))
		.pipe(rename('style-combined.css'))
		.pipe(gulp.dest(paths.dist.css));
});

gulp.task('css', function() {
	gulp.src(paths.dist.css + '/style.css')
		.pipe(autoprefixer({
			browsers: ['last 8 versions'],
			cascade: false
		}))
		.pipe(combineMq({
			beautify: true
		}))
		.pipe(cssmin())
		.pipe(rename('style.min.css'))
		.pipe(gulp.dest(paths.dist.css));
});

gulp.task('watcher', function() {
	gulp.watch(paths.src.nunjucks + '/**/*.html', ['nunjucks']);
	gulp.watch(paths.src.less + '/**/*.less', ['less']);
});

gulp.task('default', ['watcher', 'browser-sync']);