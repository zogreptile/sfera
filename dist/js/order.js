add2cart('.catalog__add2cart-btn');

// Hide popup
jQuery(function($) {
	$(document).mouseup(function(e) {
		if (!$(".order-popup").is(e.target) && 
			$(".order-popup").has(e.target).length === 0 || 
			$('#close-popup').is(e.target)) {

			$('#order-popup').css({
				'visibility': 'hidden',
				'opacity': '0'
			});
			$('.catalog__add2cart-btn').css('background-position-y', '0');
		}
	});
});
//End =============

function add2cart(obj) {
	$(obj).click(function(e) {
		e.preventDefault();
		$(this).css('background-position-y', '22px');

		var newTxt = $('#cart-info').data('txt-swap');
		var oldTxt = $('#cart-info').text();

		$('#cart-info').find('.icon-shopping-cart').toggleClass('icon-shopping-cart-filled');

		$('.cart-info').contents().filter(function() {
			return this.nodeType == 3
		}).each(function(){
			this.textContent = this.textContent.replace(oldTxt, newTxt);
		});

		$('#order-popup').css({
			'visibility': 'visible',
			'opacity': '1'
		});
		$('.make-order').fadeIn();
	});
}

removeOrder('.clear-order-btn');

function removeOrder (obj) {
	var object = $(obj);
	object.click(function() {
		if (isLast(object)) {
			$(this).closest('table').fadeOut(function() {
				$(this).remove();
				$('.hidden-info').fadeIn();
			})
			$('#contact-us').fadeOut(function() {
				$(this).remove();
			})
		} else {
			$(this).closest('tr').fadeOut(function() {
				$(this).remove();
			})
		}
	})
}

function isLast (obj) {
	var trs = $(obj).closest('tbody').find('tr');

	if (trs.length == 1) {
		return true;	
	} else {
		return false;
	} 
}
