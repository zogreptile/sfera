$(document).ready(function () {
	function preloadForm (callback) {
		$('.select2-hidden-accessible').each(function(i, obj) {
			if( $(this).attr("name") != "brand" && 
				$(this).attr("name") != "sparegroup" ) {
				$(this).parent().css('display', 'none')
			}
		});
		callback();
	}
	preloadForm(function() {
		setTimeout(function() {
			$('.form-preloader').css('display', 'none');
			$('.form-filter__inner-wrap').css('display', 'block');
		}, 1250)
	})
});

var names = ['brand', 'sparegroup'];

function initSelect2 (arg) {
	$(arg).select2({
		minimumResultsForSearch: Infinity
	})
	.on("change", function(e) {
		$(this).closest('.filter__select-wrap')
			   .find('.filter__clear')
			   .css('display', 'block');
	});
};


$('.filter__clear').click(function() {
	var filters = $('.filter__select-wrap');
	var currIndex = $(this).closest('.filter__select-wrap').index();
	var filtersArr = jQuery.makeArray(filters);

	for (i = currIndex-1; i < filters.length; i++) {
		$(filtersArr[i])
		   .find('.filter-select')
		   .val('All')
		   .trigger('change');
		$(filtersArr[i]).find('.filter__clear').css('display', 'none');
		if (jQuery.inArray($(filtersArr[i]).find('.filter-select').attr('name'), names) == -1) {
			$(filtersArr[i]).css('display', 'none');
		}
	}
});

$('#art-input').on('input', function() {
	if ($(this).val().length > 0) {
		$('#art-search-clear').css('display', 'block');
	} else {
		$('#art-search-clear').css('display', 'none');
	}
});

$('#art-search-clear').click(function() {
	$('#art-input').val('');
	$(this).css('display', 'none');
});