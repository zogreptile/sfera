function digitalClock() {
	var date = new Date();
	var hours = (date.getUTCHours() + 10) % 24;
	var minutes = date.getUTCMinutes();
	if (hours < 10) hours = "0" + hours;
	if (minutes < 10) minutes = "0" + minutes;
	$(".clock").html(hours + ":" + minutes);
	setTimeout("digitalClock()", 1000);
}

$(window).load(function () {
	var browserWindow = $(window);
	var footerHeight = $('#page-footer').outerHeight();
	
	$('#go-to-top-wrap').css('bottom', footerHeight + 39);
	browserWindow.resize(function () {
		var footerHeight = $('#page-footer').outerHeight();
		$('#go-to-top-wrap').css('bottom', footerHeight + 39);
	});
});

$(document).ready(function () {
	digitalClock();

	$('#thank-you').click(function () {
		$(this).css({
			'visibility': 'hidden',
			'opacity': '0'
		});
	});

	var browserWindow = $(window);

	var contentWidth = $('.page-container-bg').outerWidth();
	var browserWidth = $(document).width();
	//Gears Init Position
	$('#gears').css('width', (browserWidth - contentWidth) / 2);

	$('#go-to-top-wrap').css({
		'width': (browserWidth - contentWidth) / 2 + 'px'
	});


	$(this).scroll(function () {
		var y = $(this).scrollTop();
		
		y > 180 ?
			$('#go-to-top-wrap').css('opacity', '1') :
			$('#go-to-top-wrap').css('opacity', '0');
	});

	//Mobile Menu ===================
	$('#mobile-menu-btn').click(function () {
		$('.nav-list').toggleClass('nav-list--show');
	});

	$(document).mouseup(function (e) {
		if (
			!$(".nav-list").is(e.target) &&
			$(".nav-list").has(e.target).length === 0 &&
			$('.nav-list').hasClass('nav-list--show') &&
			!$('#mobile-menu-btn').is(e.target)
		) {
			$('.nav-list').removeClass('nav-list--show');
		}
	});
	//Mobile Menu End================

	var pageWrap = $('#page-wrap'),
		contentWrap = $('.page-container-bg');

	if (contentWrap.height() < pageWrap.height()) {
		contentWrap.height(pageWrap.height());
	}

	var footerHeight = $('#page-footer').outerHeight();

	$('#go-to-top-wrap').css('bottom', footerHeight + 39);
	browserWindow.resize(function () {
		var footerHeight = $('#page-footer').outerHeight();
		$('#go-to-top-wrap').css('bottom', footerHeight + 39);
	});


	$('#go-to-top').click(function () {
		$('html, body').animate({
			scrollTop: 0
		}, 800);
		return false;
	});

	//Hide Placeholder===========
	$('input,textarea').focus(function () {
		$(this).data('placeholder', $(this).attr('placeholder'))
		$(this).attr('placeholder', '');
	});
	$('input,textarea').blur(function () {
		$(this).attr('placeholder', $(this).data('placeholder'));
	});
	//End Hide Placeholder=======

	//Gears & to-top position================
	browserWindow.resize(function () {
		var contentWidth = $('.page-container-bg').outerWidth();
		var browserWidth = $(document).width();
		$('#gears').css('width', (browserWidth - contentWidth) / 2);
		$('#go-to-top-wrap').css('width', (browserWidth - contentWidth) / 2);
	});
	//Gears & to-top position End============

	browserWindow.scroll(function () {

		var gear1 = $('#gear1');
		var gear2 = $('#gear2');
		var gear3 = $('#gear3');
		var gear4 = $('#gear4');
		var rotate = $(window).scrollTop();

		gear1.css('-moz-transform', 'rotate(-' + rotate / 2 + 'deg)');
		gear1.css('-webkit-transform', 'rotate(-' + rotate / 2 + 'deg)');
		gear1.css('-ms-transform', 'rotate(-' + rotate / 2 + 'deg)');
		gear1.css('-o-transform', 'rotate(-' + rotate / 2 + 'deg)');
		gear1.css('transform', 'rotate(-' + rotate / 2 + 'deg)');

		gear2.css('-moz-transform', 'rotate(' + (rotate / 0.8) + 'deg)');
		gear2.css('-webkit-transform', 'rotate(' + rotate / 0.8 + 'deg)');
		gear2.css('-ms-transform', 'rotate(' + rotate / 0.8 + 'deg)');
		gear2.css('-o-transform', 'rotate(' + rotate / 0.8 + 'deg)');
		gear2.css('transform', 'rotate(' + rotate / 0.8 + 'deg)');

		gear3.css('-moz-transform', 'rotate(-' + rotate / 2 + 'deg)');
		gear3.css('-webkit-transform', 'rotate(-' + rotate / 2 + 'deg)');
		gear3.css('-ms-transform', 'rotate(-' + rotate / 2 + 'deg)');
		gear3.css('-o-transform', 'rotate(-' + rotate / 2 + 'deg)');
		gear3.css('transform', 'rotate(-' + rotate / 2 + 'deg)');

		gear4.css('-moz-transform', 'rotate(' + rotate + 'deg)');
		gear4.css('-webkit-transform', 'rotate(' + rotate + 'deg)');
		gear4.css('-ms-transform', 'rotate(' + rotate + 'deg)');
		gear4.css('-o-transform', 'rotate(' + rotate + 'deg)');
		gear4.css('transform', 'rotate(' + rotate + 'deg)');
	});

	//Ask popup=============
	$('#open-ask-popup').click(function () {
		$('#ask-popup').toggleClass('popup-overlay--open');
	})

	// Hide popup
	jQuery(function ($) {
		$(document).mouseup(function (e) {
			if (!$(".popup-overlay__inner").is(e.target) &&
				$(".popup-overlay__inner").has(e.target).length === 0 ||
				$('#close-ask-popup').is(e.target)) {
				$('#ask-popup').removeClass('popup-overlay--open');
			}
		});
	});
	//End Ask popup=============

	//side catalog toggle=============
	$('.side-catalog__toggle').click(function() {
		$(this).toggleClass('side-catalog__toggle--active');

		$(this).siblings('.side-catalog__sublist').toggle();

		$(this)
			.closest('.side-catalog__list-item')
			.toggleClass('side-catalog__list-item-dropdown--open');
	})

	$('.side-catalog__title > .side-catalog__toggle').click(function() {
		// $(this).toggleClass('side-catalog__toggle--active');

		console.log(this);

		$(this)
			.closest('.side-catalog')
			.find('.side-catalog__list')
			.toggle();

		$(this)
			.closest('.side-catalog__list-item')
			.toggleClass('side-catalog__list-item-dropdown--open');
	});
	//end side catalog toggle=============
});